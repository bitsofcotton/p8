# p8
The combination of the predictions.

# Description
If we take {0,1,2}-valued function as maximum variables, we get up to 16 depth tri-color tree.
So we decompose input stream to such structures with inverse with context-depend compressions, then, predict with leaf is pseudo Lebesgue measurable continuous ones.

# Why we need this
Once p0 has a almost complete prediction on the stream, the selection of the hypothesis continuous had be switched we think (or, our terminal is infected).
So if this is the switch from the world itself, we think this can be the second one with using the world compression observation context if they're variable.

# The hypothesis this condition
We make the hypothesis there's no tanglement on variable to variable, so they're completely separable, however, the value itself has some of the representations on the number as a context.

Also, we make the hypothesis represented leaf of them are enough Lebesgue measurable continuous ones.

# Don't implement now
Because p1+p0 has half compatible to this structure, and this can break some of the continuous condition on the world, we don't implement this structure.

# Ongoing our end of the implementation
This is the p\*-series our end of the implementation if randtools is enough meaningful.

